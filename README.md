# CI-Scripts

This repo includes CI scripts, that are used in our different repositories to unify the Continuous Integration process
on all repositories.

## Structure
Currently, our CI is splitt up in three stages, which process in the following order:
* Static Analysis: analyze code, without building or running it
* Build:           build code to a format which can be used in appliance
* Deploy:          deploy the build artifacts to a server they can be used from

## Configuration files
* .pylintrc: configures pylint

## Environment Variables
This Script uses Environment Variables to authentication:
* PIP_EXTRA_INDEX_URL : extra URL of our own Gitlab Package Registry including credentials to install packages
* TWINE_USERNAME : username to upload packages to our own Gitlab Package Registry
* TWINE_PASSWORD : password to upload packages to our own Gitlab Package Registry

## Built With

* [pylint](https://pylint.readthedocs.io/en/latest/) - a static code analyser for Python 2 or 3
* [yamllint](https://yamllint.readthedocs.io/en/stable/) - a linter for YAML files

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull 
requests to us.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Related resources
* [Example for auto-deploy of 
gitlab pages](https://stackoverflow.com/questions/48223039/gitlab-pages-docs-generated-with-sphinx)
* [Packaging and building a python project](https://packaging.python.org/en/latest/tutorials/packaging-projects/)
* [General Gitlab CI Documentation](https://docs.gitlab.com/ee/ci/)
